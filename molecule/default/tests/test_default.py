import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_procserv_available(host):
    assert host.run_test("manage-procs")


def test_config_directory(host):
    assert host.file("/etc/conserver").is_directory
